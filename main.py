"""
 This is a cloud functions with firebase connection

"""
import firebase_admin
import geohash
import requests
import time


from datetime import datetime
from firebase_admin import storage,db
from geoindex  import geo_point,utils
from flask import jsonify


firebaseConfig = {
    "apiKey": "AIzaSyBnqsfx7XY8XN82Vd-77ahu1De1rJTXlZ8",
    "authDomain": "kilimanjaro-355fa.firebaseapp.com",
    "databaseURL": "https://kilimanjaro-355fa.firebaseio.com",
    "projectId": "kilimanjaro-355fa",
    "storageBucket": "kilimanjaro-355fa.appspot.com",
    "messagingSenderId": "42807787156",
    "appId": "1:42807787156:web:4c7ab8300462dc86"
  };
firebase    = firebase_admin.initialize_app(options=firebaseConfig)
kilimanjaro_db = db.reference("")
firebase_handler = firebase_admin

GEO_HASH_GRID_SIZE = {
    1: 5000.0,
    2: 1260.0,
    3: 156.0,
    4: 40.0,
    5: 4.8,
    6: 1.22,
    7: 0.152,
    8: 0.038
}

def home_page(data,start,end):
    try:
        start, end = int(start), int(end)
        if end>start:
            data_result = query_nearby_event("geo_hashing",data["longitude"],data["latitude"],data["radius"],start,end)
            return jsonify({"data":data_result}),200
        else:
            return jsonify({"message":"wrong pagination"}),400
    except Exception as ex:
        return jsonify({"message":f"Failed to get location data {ex} "}),500


def get_all_event():
    try:
        result = {}
        result_events = kilimanjaro_db.child("events/approved").get()
        for event_id,event in result_events.items():
            result[event_id]=event
        return result
    except requests.HTTPError as error:
        print(f"Error {error}")
        raise error

def migrate_event_data():
    try:
        approved_event = kilimanjaro_db.child("locations/approved").get()
        geo_hash_db    = kilimanjaro_db.child("geo_hashing")
        count=0
        for event_id, value in approved_event.items():
            if "g" in value and "l" in value:
                data = {event_id:value.get("l")}
                geo_hash_db.child(value.get("g")).set(data)
                count+=1
        return jsonify({"message":f"Success migrated data successfully {count}"}),201
    except requests.HTTPError as error:
        return jsonify({"message":"Failed to migrate data "}),500

def query_nearby_objects(query_ref,longitude,latitude,radius):
    search_region_hashes =  get_nearest_points_dirty(geo_point.GeoPoint(latitude,longitude),radius,3)
    events               = get_all_event()
    events_location      = kilimanjaro_db.child(query_ref).get()
    all_nearby_objects = []
    for event_id,data in events_location.items():
        for hash_value in search_region_hashes:
            if data.get("g").startswith(hash_value) or data.get("g").endswith(hash_value):
                all_nearby_objects.append({event_id:events.get(event_id)})
    return all_nearby_objects


def listen_to_event():
    func_event = kilimanjaro_db.child("events/submission").listen()

def query_nearby_event(query_ref,longitude,latitude,radius,start,end):
    search_region_hashes = get_nearest_points_dirty(geo_point.GeoPoint(latitude, longitude), radius, 2)
    event_ids = []
    current_time_stamp = datetime.now().timestamp()
    for geo_hashes  in search_region_hashes:
        near_by_locations = kilimanjaro_db.child(query_ref).order_by_key().start_at(geo_hashes).get()
        get_event_by_id   = lambda e_id:kilimanjaro_db.child(f"events/approved/{e_id}").order_by_child("start").get()
        _event_ids = [val for _,v in near_by_locations.items() for val in v.keys() ]
        for v in _event_ids:
            event = get_event_by_id(v)
            if event.get("start") and event.get("start")<current_time_stamp:
                event_ids.append(event)
            if len(event_ids)>=((end-start)+1):
                return sorted(event_ids[start-1:end+1], key=lambda d: d.get("start"))
    end = min(end,len(event_ids))
    return sorted(event_ids[start-1:end+1],key=lambda d:d.get("start"))

def get_nearest_points_dirty(center_point, radius, precision,unit='km'):
    """
    return approx list of point from circle with given center and radius
    it uses geohash and return with some error (see GEO_HASH_ERRORS)
    :param center_point: center of search circle
    :param radius: radius of search circle
    :return: list of GeoPoints from given area
    """
    if unit == 'mi':
        radius = utils.mi_to_km(radius)
    grid_size = GEO_HASH_GRID_SIZE[precision]
    if radius > grid_size / 2:
        # radius is too big for current grid, we cannot use 9 neighbors
        # to cover all possible points
        suggested_precision = 0
        for precision, max_size in GEO_HASH_GRID_SIZE.items():
            if radius > max_size / 2:
                suggested_precision = precision - 1
                break
        raise ValueError(
            'Too large radius, please rebuild GeoHashGrid with '
            'precision={0}'.format(suggested_precision)
        )
    me_and_neighbors = geohash.expand(geohash.encode(center_point.latitude,center_point.longitude,3))
    return me_and_neighbors