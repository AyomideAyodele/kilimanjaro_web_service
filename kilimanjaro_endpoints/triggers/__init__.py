from flask import blueprints,jsonify,request
from kilimanjaro_service.service import *
from kilimanjaro_service.service.utils_handler import *
from kilimanjaro_service.db import session_scope

import logging

trigger_endpoint = blueprints.Blueprint('trigger_endpoint',__name__)


@trigger_endpoint.route("/kilimanjaro/event/<event_id>")
@profiler
def find_event(event_id):
    try:
        with session_scope() as session:
            result = find_event_by_id(event_id,session)
        return jsonify(result),200
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":"Exception occurred unable to execute transaction"}),400

@trigger_endpoint.route("/kilimanjaro/generate_report",methods=["POST"])
@profiler
def kilimanjaro_generate_report():
    try:
        data = request.json
        with session_scope() as session:
            generate_report(data,session)
        return jsonify({"message":"Successfully processed the request "}),200
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":f"Unable to process request {ex}"}),500

@trigger_endpoint.route("/kilimanjaro/ticket_purchasable/<user_id>/<ticket_id>")
@profiler
def kilimanjaro_validate_ticket_purchasable(user_id,ticket_id):
    try:
        with session_scope() as session:
            data = user_valid_for_ticket(user_id,ticket_id,session)
        return jsonify(data),200
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":f"Unable to process request {ex}"}),500


@trigger_endpoint.route("/kilimanjaro/categories")
@profiler
def kilimanjaro_categories():
    try:
        with session_scope() as session_context:
            result = query_categories(session_context)
        if result:
            return jsonify({"data":result,"max":len(result)}),200
        else:
            return jsonify({"data":[],"max":0}),200
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":"Couldn't fetch data"}),500

@trigger_endpoint.route("/kilimanjaro/email",methods=["POST"])
@profiler
def kilimanjaro_email():
    try:
        data = request.json
        email_trigger(data['event'],data)
        return  jsonify({"message":"Successfully sent the email to the recipient "}),200
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":"Failed to send "}),500
