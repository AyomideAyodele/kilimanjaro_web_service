from flask import blueprints,jsonify,request
from kilimanjaro_service.service import *
from kilimanjaro_service.service.catch_all_email import CatchAllEvent
from kilimanjaro_service.service.utils_handler import *
from kilimanjaro_service.db import session_scope

import logging
registering = blueprints.Blueprint('registering',__name__)
EVENT_VIEW_URL     = "http://kilimanjaro-355fa.appspot.com/kilimanjaro/event/{}"

@registering.route("/kilimanjaro/create_event",methods=['POST','PUT'])
@profiler
def kilimanjaro_create_event():
    try:
        data = request.json
        with session_scope() as session:
            create_service_event(data,session)
            if request.method == 'POST':
                catch_all_transaction(CatchAllEvent.EVENT_CREATION,
                                      event_id=data["event_id"], event_url=EVENT_VIEW_URL.format(data['event_id']),
                                      event_organizer=data['uid'],
                                      event_title=data['title'])
            else:
                catch_all_transaction(CatchAllEvent.EVENT_UPDATED,
                                      event_id=data["event_id"], event_url=EVENT_VIEW_URL.format(data['event_id']),
                                      event_organizer=data['uid'])
        return jsonify({"message":"Successfully created the event"}),201
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":f"Encountered error unable to {ex}"}),500



@registering.route("/kilimanjaro/event/deregister/<user_id>/<ticket_id>",methods=["POST"])
def event_deregister(user_id,ticket_id):
    try:
        data = request.json
        with session_scope() as session:
            deregister_event(data,session)
        return jsonify({"message":f"user {user_id} has been de-registered from the event {ticket_id}"}),200
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":f"Error unable to cancel event {ex}"}),500



@registering.route("/kilimanjaro/event/cancellation",methods=["POST"])
def event_deletion():
    try:
        data=request.json
        with session_scope() as session:
            cancel_event_registrations(data, session)
        return jsonify({"message":f"The user {data['user_id']} has cancelled the event {data['event_id']}"}),200
    except NotValidEventException as ex:
        logging.exception("message")
        return jsonify({"message": "Event you are looking for is not valid "}), 400
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message": "Unable to process request an exception occurred"}), 500

@registering.route("/kilimanjaro/register_event",methods=["POST"])
@profiler
def register_for_event():
    try:
        data = request.json
        with session_scope() as session:
            register_user_for_event(data,session)
        return jsonify({"message":"Successfully registered the user for the event"}),201
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":f"Unable to register an event {ex} "}),500