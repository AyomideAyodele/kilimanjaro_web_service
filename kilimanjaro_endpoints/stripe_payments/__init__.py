from flask import blueprints,jsonify,request
from kilimanjaro_service.service.stripe_service import *
from kilimanjaro_service.service.utils_handler import *
from kilimanjaro_service.db import session_scope

import logging

stripe_payment = blueprints.Blueprint('stripe_payment',__name__,url_prefix="/kilimanjaro/stripe_payment")


@stripe_payment.route("/test")
def test_url_prefix():
    return jsonify({"message":"Working"})


@stripe_payment.route("/",methods=["POST"])
def process_stripe_payment():
    try:
        data = request.json
        with session_scope() as session:
            response_meta = stripe_charge_transaction(data,session)
        if response_meta['status'] == StripeStatusEnum.SUCCESS:
            return jsonify(response_meta),200
        else:
            return jsonify(response_meta),400
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":f"Error occurred due to {ex}"}),500

@stripe_payment.route("/refund",methods=["POST"])
def process_stripe_refund():
    try:
        data = request.json
        with session_scope() as session:
            response = stripe_refund_transaction(data,session)
        if response['status'] == StripeStatusEnum.SUCCESS:
            return jsonify(response),200
        else:
            return jsonify(response),400
    except StripeInvalidRefundException:
        return jsonify({"message":"Unable to perform this transaction because it has already been executed"}),400

    except Exception as ex:
        exception_message = traceback.format_exc()
        logging.exception("message")
        return jsonify({"message":f"Error occurred due to {exception_message}"}),500

@stripe_payment.route("/create_customer",methods=["POST"])
def create_stripe_customer():
    try:
        data = request.json
        with session_scope() as session:
            register_stripe_customer(data,session)
        return jsonify({"message":"Successfully registered the stripe account"}),201
    except Exception as ex:
        exception_message = traceback.format_exc()
        logging.exception("message")
        return jsonify({"message":f"Error occurred due to {exception_message}"}),500



