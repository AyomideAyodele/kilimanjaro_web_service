from flask import blueprints,request
from kilimanjaro_service.service.email_service import *
from kilimanjaro_service.db import session_scope

email_service_handler = blueprints.Blueprint('email_service_handler',__name__,url_prefix="/kilimanjaro/email")


@email_service_handler.route("/user_registration/<event_id>/<unique_id>/<user_id>")
def notify_user_registration(event_id,unique_id,user_id):
    with session_scope() as session:
        request_method = request.method
        result = registration_notification(event_id, user_id, unique_id, request_method, session, EmailNotificationEvents.USER_REGISTRATION)
    return result


@email_service_handler.route("/user_deregistration/<event_id>/<unique_id>/<user_id>/<cancellation_id>")
def notify_user_deregistration(event_id,unique_id,user_id,cancellation_id):
    with session_scope() as session:
        request_method = request.method
        result = deregistration_notification(event_id, user_id, unique_id, cancellation_id, request_method, session)
    return result

@email_service_handler.route("/organizer_event_notification/<event_id>/<unique_id>/<user_id>/<mail_type>",defaults={"cancellation_id":""})
@email_service_handler.route("/organizer_event_notification/<event_id>/<unique_id>/<user_id>/<mail_type>/<cancellation_id>")
def notify_organizer_user_registration(event_id,unique_id,user_id,mail_type,cancellation_id):
    with session_scope() as session:
        request_method = request.method
        if mail_type==EmailNotificationEvents.ORGANIZER_USER_REGISTRATION:
            result = registration_notification(event_id, user_id, unique_id, request_method, session, mail_type)
        else:
            result = deregistration_notification(event_id,user_id,unique_id,cancellation_id,request_method,session,mail_type)
    return result

@email_service_handler.route("/report")
def send_attendance_report():
    with session_scope() as session:
        generate_event_report(session)
    return "Report completed"


