from flask import blueprints,request,jsonify
from kilimanjaro_service.service.payment_handler import *

import traceback

event_payment_handler_endpoint = blueprints.Blueprint('event_payment_handler_endpoint',__name__)



@event_payment_handler_endpoint.route("/kilimanjaro/event_payment_handler",methods=["POST"])
def payment_handler_for_event():
    try:
        data = request.json
        result = process_event_payment(data)
        return jsonify(result)
    except Exception :
        exception_string = traceback.format_exc()
        print(exception_string)
        return jsonify({"message":f"Unable to register user {exception_string}"}),500
