from flask import blueprints,jsonify,request
from kilimanjaro_service.service import *
from kilimanjaro_service.service.utils_handler import *
from kilimanjaro_service.db import session_scope

import logging
search_endpoint = blueprints.Blueprint('search_endpoint',__name__)


@search_endpoint.route("/kilimanjaro/<radius>/<longitude>/<latitude>/<page>/<size>",defaults={'start_time':0,'end_time':0})
@search_endpoint.route("/kilimanjaro/<radius>/<longitude>/<latitude>/<page>/<size>/<start_time>/<end_time>")
@profiler
def home_page_query(radius,longitude,latitude,page,size,start_time,end_time):
    return __home_page_query(radius,longitude,latitude,page,size,start_time=start_time,end_time=end_time)

@search_endpoint.route("/kilimanjaro/search/<search_word>/<page>/<size>")
@profiler
def kilimanjaro_search_word(search_word,page,size):
    try:
        data = {}
        data["search_word"]=search_word
        data["page"]       =int(page)
        data["size"]       =int(size)
        with session_scope() as session:
            result = search_pagination(data,session)
        return jsonify({"data":result,"max":len(data)}),200
    except Exception as ex:
        logging.exception("message") 
        return jsonify({"exception":f"Unable to process request {ex}"}),500

@search_endpoint.route("/kilimanjaro/search_suggestion/<search_word>")
@profiler
def search_suggestioon(search_word):
    try:
        data={}
        data["search_word"]=search_word
        result={}
        with session_scope() as session:
            result = search_by_suggestion(data,session)
        return jsonify(result),200
    except Exception as ex:
        return jsonify({"message":"Unable to search the words "}),500


@search_endpoint.route("/kilimanjaro/tickets/<user_id>")
@profiler
def kilimanjaro_user_event(user_id):
    try:
        with session_scope() as session:
            tickets=get_user_tickets(user_id,session)
        return jsonify({"data":tickets}),200
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":"Unable to process request"}),500


@search_endpoint.route("/kilimanjaro/ticket/<unique_id>/<user_id>")
@profiler
def kilimanjaro_ticket_unique_id(unique_id,user_id):
    try:
        data = dict()
        with session_scope() as session:
            data = get_ticket(unique_id,user_id,session)
        return jsonify(data),200
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":f"Unable to process request {ex}"}),500

@search_endpoint.route("/kilimanjaro/tagging/<radius>/<longitude>/<latitude>/<search_word>/<page>/<size>",defaults={'start_date':0,'end_date':0})
@search_endpoint.route("/kilimanjaro/tagging/<radius>/<longitude>/<latitude>/<search_word>/<page>/<size>/<start_date>/<end_date>")
@profiler
def kilimanjaro_search_tagging(radius,longitude,latitude,search_word,page,size,start_date,end_date):
    try:
        data={}
        data["radius"]      = 300 if float(radius)>300 else float(radius)
        data["longitude"]   = float(longitude)
        data["latitude"]    = float(latitude)
        data["page"]        = int(page)
        data["size"]        = int(size)
        data["start_time"]  = int(start_date)
        data["end_time"]    = int(end_date)
        data["search_word"]=search_word
        result = []
        with session_scope() as session:
            result = search_tag(data,session)
        return jsonify({'data':result,'max':len(result)}),200
    except Exception as ex:
        logging.exception("message")
        return jsonify({"message":"exception unable to load"}),500



def __home_page_query(radius,longitude,latitude,page,size,start_time=None,end_time=None):
    try:
        data={}
        data["radius"]=300 if float(radius)>300 else float(radius)
        data["longitude"]=float(longitude)
        data["latitude"]=float(latitude)
        data["page"]=int(page)
        data["size"]=int(size)
        data["start_time"]= int(start_time) if start_time else start_time
        data["end_time"]=int(end_time) if end_time else end_time
        result = []
        with session_scope() as session:
            result = home_page_search(data,session)
        result = {"data":result,"max":len(result)}
        return jsonify(result),200
    except Exception as ex:
        logging.exception("message")
        return jsonify({"exception":f"Unable to process request {ex}"}),500