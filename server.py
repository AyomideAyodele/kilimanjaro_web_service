import os

from flask import Flask
from kilimanjaro_service.db import load_db
from kilimanjaro_endpoints import registering, search_endpoint, \
    trigger_endpoint,stripe_payment,email_service_handler,event_payment_handler_endpoint


db_user         = os.environ.get("DB_USER")
db_pass         = os.environ.get("DB_PASS")
db_name         = os.environ.get("DB_NAME")
db_host         = os.environ.get("DB_HOST")
environment     = os.environ.get("ENV")
connection_name = os.environ.get("CLOUD_SQL_CONNECTION_NAME")

load_db(db_user,db_pass,db_name,db_host=db_host,connection_name=connection_name,env=environment)
app = Flask(__name__)
app.register_blueprint(registering)
app.register_blueprint(trigger_endpoint)
app.register_blueprint(search_endpoint)
app.register_blueprint(event_payment_handler_endpoint)
app.register_blueprint(stripe_payment)
app.register_blueprint(email_service_handler)

if __name__ == '__main__':
    app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    app.run(debug=True)