
class InvalidRegistration(Exception):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)

class ResultNotFound(Exception):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)