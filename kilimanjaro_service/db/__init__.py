import os
from sqlalchemy.ext.declarative import declarative_base as __DECLARATIVE_Base__
from contextlib import contextmanager
from sqlalchemy import create_engine,text
from sqlalchemy.engine import url
from sqlalchemy.orm import sessionmaker

Base = __DECLARATIVE_Base__()

class DatabaseException(Exception):
    def __init__(self,*args,**kwargs):
        super().__init__(args,kwargs)


Session = None

def load_db(db_user,db_pass,db_name,db_host="",connection_name="",env="DEV"):
    if env=="DEV":
        url_str = url.URL(drivername='mysql+mysqldb', username=db_user,
                          password=db_pass, database=db_name,
                          host=db_host,query={"charset":"utf8"})
    else:
        url_str = url.URL(drivername='mysql+mysqldb',username=db_user,
                                       password=db_pass,database=db_name,query={"unix_socket":"/cloudsql/{}".format(connection_name),"charset":"utf8"})
    __engine__ = create_engine(url_str, echo=False)
    global Session
    Session = sessionmaker(bind=__engine__)
    Base.metadata.create_all(__engine__)


class DbRepo:
    def __init__(self,session,cls):
        self.session = session
        self.__cls   = cls

    def update_insert(self,user):
        self.session.add(user)


    def select_all(self):
        return self.session.query(self.__cls).all()

    def execute_query(self,**kwargs):
        try:
            return self.session.\
                query(self.__cls).filter_by(**kwargs).all()
        except Exception as e:
            print(e)
            return None

    def execute_query_for_one(self,**kwargs):
        try:
            return  self.session.\
            query(self.__cls).filter_by(**kwargs).one()
        except Exception as e:
            print(e)
            return None


    def find_id(self,id):
        return self.session.\
            query(self.__cls).get(id)


def execute_raw_select_query(session,query:str,columns:tuple,parameter:dict=None):
    if parameter is None:
        sql_query  = text(query)
        sql_result = session.execute(sql_query)
    else:
        sql_query  = text(query)
        sql_result = session.execute(sql_query,parameter)
    result = []
    for row in sql_result:
        parsed_row = [value if value else "" for value in row]
        row_data = dict(zip(columns, parsed_row))
        result.append(row_data)
    return result

def create_table(session):
    pass

@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    if Session:
        session = Session()
        try:
            yield session
            session.commit()
        except DatabaseException as e:
            session.rollback()
            raise e
        finally:
            session.close()

