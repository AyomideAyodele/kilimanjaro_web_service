from kilimanjaro_service.db import Base
from kilimanjaro_service.db.model import EntityJson
from sqlalchemy import *
from sqlalchemy.orm import relationship
from datetime import datetime

class StripeCustomer(Base,EntityJson):
    __tablename__       = "stripe_customers"
    pk_id               = Column(INTEGER,primary_key=True,name="stripe_customer_pk_id")
    stripe_customer_id  = Column(VARCHAR)
    customer_unq_id     = Column(VARCHAR)

    def __init__(self,stripe_customer_id,customer_unq_id):
        self.stripe_customer_id = stripe_customer_id
        self.customer_unq_id    = customer_unq_id


class StripeTransaction(Base,EntityJson):
    __tablename__       = "stripe_transactions"
    pk_id               = Column(INTEGER,primary_key=True,name="stripe_transaction_pk_id")
    source              = Column(VARCHAR)
    destination         = Column(VARCHAR)
    time_executed       = Column(DATETIME)
    charge_type         = Column(VARCHAR)
    transaction_id      = Column(VARCHAR)
    request             = Column(TEXT)
    response            = Column(TEXT)
    status              = Column(VARCHAR)
    unique_id           = Column(VARCHAR)
    customer_stripe_id  = Column(Integer, ForeignKey("stripe_customers.stripe_customer_pk_id"), name="stripe_customers_stripe_customer_pk_id")
    stripe_customer     = relationship("StripeCustomer", backref="stripe_transaction")

    def __init__(self,source,destination,charge_type,transaction_id,customer_stripe_id,request,response,status,stripe_customer,unique_id):
        self.source         = source
        self.destination    = destination
        self.charge_type    = charge_type
        self.time_executed  = datetime.now()
        self.transaction_id = transaction_id
        self.customer_stripe_id = customer_stripe_id
        self.request        = request
        self.response       = response
        self.status         = status
        self.stripe_customer = stripe_customer
        self.unique_id       = unique_id
