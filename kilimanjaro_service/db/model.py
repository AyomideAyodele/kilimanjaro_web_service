from kilimanjaro_service.db import Base,DbRepo
from kilimanjaro_service.service.countries import *
from kilimanjaro_service.db.db_exception import *
from decimal import Decimal
from sqlalchemy import *
from sqlalchemy.orm import relationship,backref
from sqlalchemy.orm.exc import *
from sqlalchemy.ext.hybrid import hybrid_method
from kilimanjaro_service.service.service_handler import *
from kilimanjaro_service.service.utils_handler import *
from datetime import datetime

import json
import math
import uuid


Earth_radius_km = 6371.009
km_per_deg_lat = 2 * math.pi * Earth_radius_km / 360.0


EVENT_COLUMN_ORDERING = ("event_pk_id","event_id_firebase","event_title_name","event_description","event_main_image","geohash","event_json", "event_start_time","event_end_time","country_tag","VALID_EVENT","event_cancellation_num","event_cancellation_time","longitude","latitude")

class EntityJson:
    def json_dict(self):
        data = self.__dict__
        data.pop("_sa_instance_state")
        return data


class Event(Base,EntityJson):
    __tablename__           ="event"
    event_pk_id             = Column(Integer, primary_key=True,autoincrement=True)
    event_id_firebase       = Column(VARCHAR)
    event_title_name        = Column(TEXT)
    event_main_image        = Column(TEXT)
    event_description       = Column(Text)
    geohash                 = Column(VARCHAR)
    event_blob_json         = Column(TEXT,name="event_json")
    event_start_time        = Column(DATETIME)
    event_end_time          = Column(DATETIME)
    country_tag             = Column(VARCHAR)
    event_cancellation_num  = Column(VARCHAR)
    event_cancellation_time = Column(DATETIME)
    longitude               = Column(FLOAT)
    latitude                = Column(FLOAT)
    event_tags          = relationship("Event_tagging",secondary="tagging_event_table")
    valid_event         = Column(BOOLEAN,name="VALID_EVENT") #TODO: Means the event is cancelled or not

    def __init__(self):
        pass

    def create_event_using_dict(self,data):
        self.event_pk_id=data["event_pk_id"]
        self.event_id_firebase = data["event_id_firebase"]
        self.event_title_name = data["event_title_name"]
        self.event_main_image = data["event_main_image"]
        self.event_description = data["event_description"]
        self.event_blob_json = data["event_json"]
        self.event_start_time = data["event_start_time"]
        self.event_end_time = data["event_end_time"]
        self.country_tag = data["country_tag"]
        self.event_cancellation_num = data["event_cancellation_num"]
        self.event_cancellation_time = data["event_cancellation_time"]
        self.longitude = data["longitude"]
        self.latitude = data["latitude"]
        self.valid_event = data["VALID_EVENT"]


    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return self.__str__()

    def __hash__(self):
        return self.event_pk_id

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    @hybrid_method
    def compare_distance(self,longitude,longitude1,latitude,latitude1,radius):
        if not longitude1 and not  latitude1:
            return False
        distance = km_per_deg_lat * math.sqrt((latitude1 - latitude) ** 2 + (longitude1 - longitude) ** 2)
        return distance<=radius

    def distance_from_location(self,longitude,latitude):
        return km_per_deg_lat * math.sqrt((self.latitude - latitude) ** 2 + (self.longitude - longitude) ** 2)

    def event_json(self):
        try:
            event_str_json = self.event_blob_json.replace("'",'"')
            data = json.loads(event_str_json)
            return data
        except Exception as ex:
            return dict()

    def build_event(self,data):
        if data.get("country",""):
            print(f"Country found {data.get('country')}")
        if self.event_json():
            event_data             = self.event_json()
            if not "image" in event_data:
                event_data['image']='https://storage.googleapis.com/kilimanjaro-355fa.appspot.com/images/kilimanjaro1.jpg'
                event_data['image_location']='images/kilimanjaro1.jpg'
                self.event_blob_json=str(event_data)
        self.event_id_firebase = data["event_id"]
        self.event_title_name  = data["title"]
        self.event_main_image  = data.get("image","https://storage.googleapis.com/kilimanjaro-355fa.appspot.com/images/kilimanjaro1.jpg")
        self.geohash           = data["geo_hash"]
        self.event_description = data["description"]
        self.event_blob_json   = json.dumps(data).encode("utf-8")
        self.event_start_time  = convert_datetime_from_str(data["start"]) if isinstance(data["start"],str) else datetime.fromtimestamp(data["start"]/1000)
        self.event_end_time    = convert_datetime_from_str(data["end"]) if isinstance(data["end"],str) else datetime.fromtimestamp(data["end"]/1000)
        self.country_tag       = african_countries.get(data.get("country","aa"))
        self.longitude         = data["longitude"]
        self.latitude          = data["latitude"]
        self.valid_event       = True
class Event_tagging(Base,EntityJson):
    __tablename__     = "event_tagging"
    event_tagging_id  = Column(INTEGER,primary_key=True,autoincrement=True)
    tagging_image_url = Column(TEXT)
    tagging_content   = Column(TEXT,name="event_tagging_content")
    events             = relationship("Event",secondary="tagging_event_table")



class Event_And_Tagging(Base,EntityJson):
    __tablename__     = "tagging_event_table"
    __table_args__    = (PrimaryKeyConstraint("event_pk_id","event_tagging_id"),)
    event_pk_id       = Column(Integer,ForeignKey("event.event_pk_id"))
    event_tagging_id  = Column(Integer,ForeignKey("event_tagging.event_tagging_id"))
    events            = relationship("Event", backref=backref("tagging_event_table", cascade="all, delete-orphan"))
    tagging           = relationship("Event_tagging",backref=backref("tagging_event_table", cascade="all, delete-orphan"))

class Event_Registration(Base,EntityJson):
    __tablename__               = "event_registration"
    reg_id                      = Column(Integer,primary_key=True,autoincrement=True)
    reg_unique_id               = Column(VARCHAR,name="reg_unq_id",unique=True)
    reg_first_name              = Column(VARCHAR)
    reg_last_name               = Column(VARCHAR)
    reg_email                   = Column(VARCHAR)
    reg_phone_number            = Column(VARCHAR)
    quantity                    = Column(Integer)
    user_info_id                = Column(Integer,ForeignKey("user_info.user_id"),name="user_id")
    event_payment_id            = Column(Integer,ForeignKey("event_payment.event_payment_id"))
    event_ticket_id             = Column(Integer,ForeignKey("event_ticket.ticket_pk_id"))
    event_refund_id             = Column(Integer,ForeignKey("event_refund.event_refund_id"))
    event_ticket                = relationship("Event_Ticket",backref="event_registration")
    event_payment               = relationship("Event_Payment", backref="event_registration")
    event_refund                = relationship("Event_Refund", backref="event_registration")
    event_reg_datetime          = Column(DATETIME)
    event_cancellation_id       = Column(VARCHAR)
    event_cancellation_datetime = Column(DATETIME)
    event_status        = Column(VARCHAR)
    user_firebase_id    = Column(VARCHAR)
    transaction_id      = Column(VARCHAR)

    @profiler
    def build_registration(self,data,event_name,count_transaction,transaction_id,user_id,quantity):
        self.reg_unique_id   = generate_event_uid(event_name,count_transaction)
        self.transaction_id  = transaction_id
        self.reg_first_name  = data['first_name']
        self.reg_last_name   = data['last_name']
        self.event_reg_datetime = datetime.fromtimestamp(float(data['timestamp'])/1000)
        if 'email' in data:
            self.reg_email = data['email']
        if 'phone_number' in data:
            self.reg_phone_number = data['phone_number']
        self.event_status   = EventStatus.ATTENDING
        self.user_firebase_id = user_id
        self.quantity         = quantity




class Event_Payment(Base,EntityJson):
    __tablename__           = "event_payment"
    event_payment_id        = Column(Integer,primary_key=True,autoincrement=True)
    stripe_customer_id      = Column(VARCHAR)
    confirmation_number     = Column(VARCHAR)
    paid_amount             = Column(DECIMAL)
    status                  = Column(VARCHAR)
    payment_datetime        = Column(DATETIME)

    @profiler
    def build_event_payment(self,data,transaction_id):
        self.stripe_customer_id   = data['customer_id']
        self.confirmation_number  = transaction_id # Currently using the stripe_transaction_id
        self.paid_amount          = Decimal(data['amount'])
        self.status               = PaymentStatus.COMPLETED
        self.payment_datetime     =  datetime.fromtimestamp(data['payment_date_time']/1000)

class Event_Ticket(Base,EntityJson):
    __tablename__           = "event_ticket"
    ticket_pk_id            = Column(Integer,primary_key=True,autoincrement=True)
    ticket_id_firebase      = Column(VARCHAR)
    ticket_title            = Column(VARCHAR)
    ticket_type             = Column(VARCHAR)
    ticket_quantity         = Column(INTEGER)
    ticket_purchasable      = Column(INTEGER)
    ticket_price            = Column(DECIMAL)
    ticket_datetime         = Column(DATETIME)
    event_id                = Column(Integer, ForeignKey("event.event_pk_id"))
    event                   = relationship("Event", backref="event_tickets")
    ticket_sold             = Column(Integer)

    def __init__(self):
        self.ticket_sold = 0

    @profiler
    def build_event_ticket(self,data,ticket_id_firebase):
        self.ticket_id_firebase = ticket_id_firebase
        self.ticket_title       = data.get("title","")
        if data["type"].lower() == "free":
            self.ticket_type = PaymentType.FREE
        elif data["type"].lower() == "paid":
            self.ticket_type  = PaymentType.PAID
            self.ticket_price = data.get("price")
        else:
            self.ticket_type = PaymentType.DONATION
            self.ticket_price = data.get("price",0.0)
        self.ticket_quantity    = data["quantity"]
        self.ticket_purchasable = data["purchasable"]
        self.ticket_datetime    = datetime.now()



class User_Info(Base,EntityJson):
    __tablename__ = "user_info"
    user_id       = Column(Integer,primary_key=True,autoincrement=True)
    user_name     = Column(VARCHAR)
    password      = Column(VARCHAR)
    firstname     = Column(VARCHAR)
    last_name     = Column(VARCHAR)
    user_unq_id   = Column(VARCHAR)
    user_email    = Column(VARCHAR)
    event         = relationship("Event_Registration",backref="user_info")

class UserEventRelation(Base,EntityJson):
    __tablename__ = "user_event_relation"
    user_event_id = Column(Integer,primary_key=True,autoincrement=True)
    user_unq_id   = Column(VARCHAR)
    event_unq_id  = Column(VARCHAR)
    create_date_time = Column(DATETIME)
    valid_event     = Column(BOOLEAN)

    def build_user_event_relation(self,user_unq_id,event_unq_id):
        self.user_unq_id        = user_unq_id
        self.event_unq_id       = event_unq_id
        self.create_date_time   = datetime.now()
        self.valid_event        = True


class Event_Refund(Base,EntityJson):
    __tablename__    = "event_refund"
    event_refund_id  = Column(Integer, primary_key=True, autoincrement=True)
    event_refund_amt = Column(DECIMAL)
    event_amt        = Column(DECIMAL)
    refund_unq_id    = Column(VARCHAR)
    refund_datetime  = Column(DATETIME)
    refund_status    = Column(VARCHAR)

@profiler
def create_event(data,db_session):
    if not "taggings" in data:
        taggings = DbRepo(db_session,Event_tagging).execute_query_for_one(tagging_content="Others")
        if taggings:
            data["taggings"]=[taggings.event_tagging_id]
    event = DbRepo(session=db_session,cls=Event).execute_query_for_one(event_id_firebase=data["event_id"])
    user_event = DbRepo(session=db_session,cls=UserEventRelation).execute_query_for_one(event_unq_id=data["event_id"])
    if not user_event:
        user_event=UserEventRelation()
        user_event.build_user_event_relation(data["uid"],data["event_id"])
        db_session.add(user_event)
    event = event if event and event.valid_event else None
    if not event:
        event = Event()
    event.build_event(data)
    event_firebase_json = data
    for ticket_id,ticket_data in event_firebase_json["tickets"].items():
        ticket = DbRepo(session=db_session,cls=Event_Ticket).execute_query_for_one(ticket_id_firebase=ticket_id)
        if ticket:
            ticket.build_event_ticket(ticket_data,ticket_id)
        else:
            ticket = Event_Ticket()
            ticket.build_event_ticket(ticket_data,ticket_id)
        event.event_tickets.append(ticket)
    for tag in data["taggings"]:
        if isinstance(data["taggings"],dict):
            tag_id=data["taggings"][tag]
        else:
            tag_id=tag
        event_tagging = DbRepo(session=db_session, cls=Event_tagging).execute_query_for_one(
            event_tagging_id=tag_id)
        if event_tagging:
            event_tagging.events.append(event)
    db_session.add(event)


@profiler
def find_event_by_ticket_firebase_id(ticket_firebase_id,session):
    ticket = DbRepo(session,Event_Ticket).execute_query_for_one(ticket_id_firebase=ticket_firebase_id)
    event  = ticket.event
    return event

@profiler
def create_event_registration(data,db_session):
    ticket_id   = data['ticket_id']
    transaction = data['charge_id']
    user_id     = data['user_id']
    ticket      = DbRepo(db_session,Event_Ticket)\
        .execute_query_for_one(ticket_id_firebase=ticket_id)
    users_registrations = db_session.query(Event_Registration).\
        filter(and_(Event_Registration.user_firebase_id==user_id,Event_Registration.event_ticket_id==ticket.ticket_pk_id,Event_Registration.event_status==EventStatus.ATTENDING)).count()
    if ticket.ticket_purchasable-users_registrations<data['quantity'] or ticket.ticket_sold>=ticket.ticket_quantity:
       raise InvalidRegistration()
    event     = ticket.event
    result    = []
    registrations = []
    for registration_data in data["registrations"]:
        event_data = dict()
        count    = db_session.query(Event_Registration).count()
        register = Event_Registration()
        register.build_registration(registration_data,event.event_title_name,
                                    count+1,transaction,user_id,data['quantity'])
        ticket.event_registration.append(register)
        event_json               = event.event_json()
        event_data['first_name'] = registration_data["first_name"]
        event_data['last_name']  = registration_data["last_name"]
        event_data['to_email']      = registration_data["email"]
        event_data['title']      = event.event_title_name
        event_data['quantity']   = data['quantity']
        event_data['u_id']       = data['user_id']
        event_data['event_id']   = event.event_id_firebase
        event_data["organizer_email"] = event_json.get("email","")
        event_data['unique_id']  = register.reg_unique_id
        event_data['date_time']  = event.event_start_time.strftime("%b-%d-%Y").replace("-"," ")
        event_data['place']      = event.event_json().get("address","")
        event_data['paid']       = ticket.ticket_type==PaymentType.PAID
        event_data['ticket_type'] = ticket.ticket_type
        if event_data['paid']:
            event_data['amount'] = ticket.ticket_price
        result.append(event_data)
        db_session.add(register)
        registrations.append(register)
    event_dict = event.event_json()
    event_dict["tickets"][ticket_id]['quantity']=event_dict["tickets"][ticket_id]['quantity']-data['quantity']
    event.event_blob_json = str(event_dict)
    ticket.ticket_sold+=data['quantity']
    if data['paid']:
        payment = Event_Payment()
        payment.build_event_payment(data,data['transaction_id'])
        payment.event_registration.extend(registrations)
    return result

@profiler
def search_for_event(search_word,page,size,session):
    events = __get_event_ids(session,search_word)
    data = session.query(Event).filter(
        or_(Event.event_description.startswith(search_word),
            Event.event_title_name.endswith(search_word),
            Event.country_tag==search_word, Event.event_pk_id.in_(events))).filter(Event.valid_event==True)\
        .limit(size).offset((page-1)*size).all()
    return data

@profiler
def get_event_by_id(event_id,session,valid_event=True):
    if valid_event:
        event_repo = DbRepo(session,Event).execute_query_for_one(event_id_firebase=event_id,valid_event=True)
        return event_repo
    else:
        event_repo = DbRepo(session, Event).execute_query_for_one(event_id_firebase=event_id)
        return event_repo
@profiler
def get_registration_data(reg_unique_id,user_firebase_id,session):
    try:
        data = session.query(Event_Registration).filter(and_(Event_Registration.reg_unique_id==reg_unique_id,
                                                         Event_Registration.user_firebase_id==user_firebase_id,Event_Registration.event_status==EventStatus.ATTENDING)).one()
        return data;
    except ResultNotFound as ex:
        return None
@profiler
def get_ticket_by_id(unique_id,session):
    event_registration = DbRepo(session,Event_Registration).execute_query_for_one(reg_unique_id=unique_id)
    return event_registration

@profiler
def get_registrations(user_id,ticket_id,session):
    ticket        = DbRepo(session,Event_Ticket).execute_query_for_one(ticket_id_firebase=ticket_id)
    if ticket:
        registrations = DbRepo(session,Event_Registration).execute_query(user_firebase_id=user_id,event_ticket_id=ticket.ticket_pk_id)
        return registrations
    raise ResultNotFound()

@profiler
def get_users_registration(user_id,session):
    tickets = session.query(Event_Registration).filter(Event_Registration.user_firebase_id==user_id)\
        .filter(Event_Registration.event_status==EventStatus.ATTENDING).all()
    return tickets

@profiler
def refund_event_ticket(data,session):
    event_refund =Event_Refund()
    event_refund.event_refund_amt=data["refund_amount"]
    event_refund.event_amt=data["event_amount"]
    event_refund.refund_datetime=datetime.now()
    event_refund.refund_status=RefundStatus.COMPLETED
    event_refund.refund_unq_id=data['unique_id']
    session.add(event_refund)
    return event_refund

@profiler
def event_tickets(event_ids:list,session):
    result = []
    for event_id in event_ids:
        try:
            event = session.query(Event).filter(Event.event_id_firebase==event_id).filter(Event.valid_event==True).one()
        except ResultNotFound as ex:
            continue
        result.append(event)
    return result


@profiler
def get_user_event_relation(event_id,user_id,session):
    try:
        data = session.query(UserEventRelation).filter(and_(UserEventRelation.user_unq_id==user_id,UserEventRelation.event_unq_id==event_id)).one()
        return data
    except NoResultFound as ex:
        return None



@profiler
def home_page_query(page,size,session,start_time=None,end_time=None,longitude=None,latitude=None,radius=None):
    result = []
    data=[]
    if not start_time and not end_time:
        data = __search_by_geo_location(longitude, latitude, radius, page, size, session)
    elif start_time and end_time:
        start_date = datetime.fromtimestamp(start_time/1000)
        end_date   = datetime.fromtimestamp(end_time/1000)
        data = __search_by_geo_location(longitude, latitude, radius, page, size, session,start_date,end_date)
    elif start_time:
        start_date = datetime.fromtimestamp(start_time/1000)
        data = __search_by_geo_location(longitude, latitude, radius, page, size, session,start_date)
    elif end_time:
        end_date = datetime.fromtimestamp(end_time/1000)
        start_date = datetime.now()
        data = __search_by_geo_location(longitude, latitude, radius, page, size, session,start_date,end_date)
    result.extend(data)
    return result

@profiler
def get_event_attendees(event_id,db_session):
    event = DbRepo(session=db_session,cls=Event).execute_query_for_one(event_id_firebase=event_id)
    event = event if event and event.valid_event else None
    if not event:
        raise ResultNotFound()
    event_title = event.event_title_name
    result = []
    for ticket in event.event_tickets:
        attendees = db_session.query(Event_Registration).filter(Event_Registration.event_ticket_id==ticket.ticket_pk_id)\
            .filter(Event_Registration.event_status==EventStatus.ATTENDING).all()
        result.extend(attendees)
    return (event_title,result)

@profiler
def search_by_tagging(longitude, latitude, radius,search_word,page,size,session,start_time=0,end_time=0):
    final_results = []
    page_offset = (page - 1) * size
    if not start_time and not end_time:
        start_date = str(datetime.now())
        query = text(" select e.*, SQRT(POW(69.1 * (latitude - :latitude), 2) + POW(69.1 * (:longitude - longitude) * COS(latitude / 57.3), 2)) AS distance from tagging_event_table as te, "
                     "event_tagging as et,event as e where e.valid_event =1 and et.event_tagging_id=te.event_tagging_id and e.event_pk_id=te.event_pk_id and et.event_tagging_content= :tagging and event_start_time>= :start_time HAVING distance < :radius ORDER BY distance,event_start_time LIMIT :size OFFSET :page")
        results = session.execute(query,{"radius":radius,"size":size,"page":page_offset,"longitude":longitude,"latitude":latitude,"start_time":start_date,"tagging":search_word})
        for result in results:
            data = dict(zip(EVENT_COLUMN_ORDERING, result))
            event = Event()
            event.create_event_using_dict(data)
            final_results.append(event)
        return final_results
    elif start_time:
        start_date = str(start_time)
        query = text(
            " select e.*, SQRT(POW(69.1 * (latitude - :latitude), 2) + POW(69.1 * (:longitude - longitude) * COS(latitude / 57.3), 2)) AS distance from tagging_event_table as te, "
            "event_tagging as et,event as e where e.valid_event =1 and et.event_tagging_id=te.event_tagging_id and e.event_pk_id=te.event_pk_id and et.event_tagging_content= :tagging and event_start_time>= :start_time HAVING distance < :radius ORDER BY distance,event_start_time LIMIT :size OFFSET :page")
        results = session.execute(query, {"radius": radius, "size": size, "page": page_offset, "longitude": longitude,
                                          "latitude": latitude, "start_time": start_date, "tagging": search_word})
        for result in results:
            data = dict(zip(EVENT_COLUMN_ORDERING, result))
            event = Event()
            event.create_event_using_dict(data)
            final_results.append(event)
        return final_results
    elif end_time:
        end_date = str(end_time)
        query = text(
            " select e.*, SQRT(POW(69.1 * (latitude - :latitude), 2) + POW(69.1 * (:longitude - longitude) * COS(latitude / 57.3), 2)) AS distance from tagging_event_table as te, "
            "event_tagging as et,event as e where e.valid_event =1 and et.event_tagging_id=te.event_tagging_id and e.event_pk_id=te.event_pk_id and et.event_tagging_content= :tagging and event_end_time <= :end_time HAVING distance < :radius ORDER BY distance,event_start_time LIMIT :size OFFSET :page")
        results = session.execute(query, {"radius": radius, "size": size, "page": page_offset, "longitude": longitude,
                                          "latitude": latitude, "end_time": end_date, "tagging": search_word})
        for result in results:
            data = dict(zip(EVENT_COLUMN_ORDERING, result))
            event = Event()
            event.create_event_using_dict(data)
            final_results.append(event)
        return final_results
    else:
        start_date = str(start_time)
        end_date   = str(end_time)
        query = text(
            " select e.*, SQRT(POW(69.1 * (latitude - :latitude), 2) + POW(69.1 * (:longitude - longitude) * COS(latitude / 57.3), 2)) AS distance from tagging_event_table as te, "
            "event_tagging as et,event as e where e.valid_event =1 and et.event_tagging_id=te.event_tagging_id and e.event_pk_id=te.event_pk_id and et.event_tagging_content= :tagging and event_start_time>= :start_time and event_end_time <= :end_time HAVING distance < :radius ORDER BY distance,event_start_time LIMIT :size OFFSET :page")
        results = session.execute(query, {"radius": radius, "size": size, "page": page_offset, "longitude": longitude,
                                          "latitude": latitude, "end_time": end_date, "tagging": search_word,"start_time":start_date})
        for result in results:
            data = dict(zip(EVENT_COLUMN_ORDERING, result))
            event = Event()
            event.create_event_using_dict(data)
            final_results.append(event)
        return final_results

def __search_by_geo_location(longitude, latitude, radius, page, size, session, start_date=None, end_date=None):
    if not start_date and not end_date:
        page_offset = (page-1)*size
        start_date  = str(datetime.now())
        query = text("SELECT *, SQRT(POW(69.1 * (latitude - :latitude), 2) + POW(69.1 * (:longitude - longitude) * COS(latitude / 57.3), 2)) AS distance FROM event where event_start_time>= "
                     ":start_time and valid_event=1 HAVING distance < :radius ORDER BY distance,event_start_time LIMIT :size OFFSET :page")
        results = session.execute(query,{"radius":radius,"size":size,"page":page_offset,"longitude":longitude,"latitude":latitude,"start_time":start_date})
        final_result=[]
        for result in results:
            data = dict(zip(EVENT_COLUMN_ORDERING,result))
            event = Event()
            event.create_event_using_dict(data)
            final_result.append(event)
        return final_result
    elif start_date and end_date:
        page_offset = (page-1)*size
        start_date = str(start_date)
        end_date   = str(end_date)
        query = text("SELECT *, SQRT(POW(69.1 * (latitude - :latitude), 2) + POW(69.1 * (:longitude - longitude) * COS(latitude / 57.3), 2)) AS distance FROM event where event_start_time>= "
                     ":start_time and event_end_time <= :end_time and valid_event=1 HAVING distance < :radius ORDER BY distance,event_start_time LIMIT :size OFFSET :page")
        results = session.execute(query,{"radius":radius,"size":size,"page":page_offset,"longitude":longitude,"latitude":latitude,"start_time":start_date,"end_time":end_date})
        final_result=[]
        for result in results:
            data = dict(zip(EVENT_COLUMN_ORDERING,result))
            event = Event()
            event.create_event_using_dict(data)
            final_result.append(event)
        return final_result
    elif  start_date:
        page_offset = (page-1)*size
        start_date  = str(start_date)
        query = text("SELECT *, SQRT(POW(69.1 * (latitude - :latitude), 2) + POW(69.1 * (:longitude - longitude) * COS(latitude / 57.3), 2)) AS distance FROM event where event_start_time>= "
                     ":start_time and valid_event=1 HAVING distance < :radius ORDER BY distance,event_start_time LIMIT :size OFFSET :page")
        results = session.execute(query,{"radius":radius,"size":size,"page":page_offset,"longitude":longitude,"latitude":latitude,"start_time":start_date})
        final_result=[]
        for result in results:
            data = dict(zip(EVENT_COLUMN_ORDERING,result))
            event = Event()
            event.create_event_using_dict(data)
            final_result.append(event)
        return final_result
    else:
        page_offset = (page - 1) * size
        end_date = str(end_date)
        query = text(
            "SELECT *, SQRT(POW(69.1 * (latitude - :latitude), 2) + POW(69.1 * (:longitude - longitude) * COS(latitude / 57.3), 2)) AS distance FROM event where "
            " event_end_time <= :end_time and valid_event=1 HAVING distance < :radius ORDER BY distance,event_start_time LIMIT :size OFFSET :page")
        results = session.execute(query, {"radius": radius, "size": size, "page": page_offset, "longitude": longitude,
                                          "latitude": latitude,"end_time": end_date})
        final_result = []
        for result in results:
            data = dict(zip(EVENT_COLUMN_ORDERING, result))
            event = Event()
            event.create_event_using_dict(data)
            final_result.append(event)
        return final_result


def __get_event_ids(session,search_word):
    results = session.query(Event_tagging).filter(Event_tagging.tagging_content.like(f'%{search_word}%')).all()
    events = [d.event_tagging_id for d in results]
    event_ids = session.query(Event_And_Tagging).filter(Event_And_Tagging.event_tagging_id.in_(events)).all()
    return [event.event_pk_id for event in event_ids]



@profiler
def query_categories_tag(session):
    results = DbRepo(session,Event_tagging).select_all()
    return [value.json_dict() for value in results]