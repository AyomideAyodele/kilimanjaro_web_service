import logging
import functools

from datetime import datetime

logging.getLogger().addHandler(logging.StreamHandler())

class NotValidEventException(Exception):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)

class UnableToDownloadPDF(Exception):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)

class DocumentNotFound(Exception):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)

def profiler(func):
    @functools.wraps(func)
    def decorator(*args,**kwargs):
        timestamp1 = datetime.now()
        try:
            logging.info(f"Executing the function {func.__name__} ")
            result =  func(*args,**kwargs)
            return result
        except Exception as e:
            logging.error(f"{func.__name__} Encountered an error: \n")
            logging.exception("message")
            raise e
        finally:
            timestamp2 = datetime.now()
            diff = timestamp2 - timestamp1
            second = "second's" if diff.total_seconds()>1 else "second"
            logging.info(f"{func.__name__} executed for {diff.total_seconds()} {second}")
    return decorator

def convert_datetime_from_str(data):
    datetime_object = datetime.strptime(data, '%B %d, %Y %I:%M %p')
    return datetime_object