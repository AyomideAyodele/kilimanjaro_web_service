from kilimanjaro_service.db import *
from kilimanjaro_service.db.model import *
from kilimanjaro_service.service import *
from jinja2 import Environment, FileSystemLoader

import logging

SEND_EMAIL     = "noreply@thekilimanjaroapp.com"
NOTIFICATION_EMAIL = "support@thekilimanjaroapp.com"
file_loader = FileSystemLoader('')
env = Environment(loader=file_loader)


logging.getLogger().setLevel(logging.INFO)


class EmailNotificationEvents:
    USER_REGISTRATION           = "USER_REGISTRATION"
    USER_CANCELLATION           = "USER_CANCELLATION"
    ORGANIZER_USER_REGISTRATION = "ORGANIZER_USER_REGISTRATION"
    ORGANIZER_USER_CANCELLATION = "ORGANIZER_USER_CANCELLATION"
    ORGANIZER_EVENT_DELETION    = "ORGANIZER_EVENT_DELETION"



def registration_notification(event_id, user_id, unique_ticket_id, method, session, mail_type):
    event_entity        = get_event_by_id(event_id,session,valid_event=False)
    event_registration  = get_registration_data(unique_ticket_id,user_id,session)
    if mail_type==EmailNotificationEvents.USER_REGISTRATION:
        html_content,subject = __user_registration_email_notification(event_registration.quantity,event_entity.event_title_name,unique_ticket_id)
        if method=='POST':
            send_user_registration(event_entity,event_registration,unique_ticket_id)
        return html_content
    elif mail_type==EmailNotificationEvents.ORGANIZER_USER_REGISTRATION:
        paid    = event_registration.event_status == PaymentType.PAID
        amount = 0
        if paid:
            amount  = event_registration.event_ticket.ticket_price
        html_content,subject = __organizer_email_notification(event_entity.event_title_name,event_registration.reg_first_name,
                                                              event_registration.reg_last_name,event_registration.reg_unique_id,paid,mail_type,amount)
        if method=='POST':
            send_email_with_attachement(html_content,subject,SEND_EMAIL,event_entity.event_json().get("email",""))
        return html_content


def deregistration_notification(event_id, user_id, unique_ticket_id, cancellation_id, method, session, mail_type):
    event_entity       = get_event_by_id(event_id,session,valid_event=False)
    event_registration = get_registration_data(unique_ticket_id,user_id,session)
    paid = event_registration.event_ticket.ticket_type==PaymentType.PAID
    amount = 0
    if paid:
        amount = event_registration.event_ticket.ticket_price
    if mail_type==EmailNotificationEvents.USER_CANCELLATION:
        html_content,subject = __user_cancellation_email_notification(event_entity.event_title_name,cancellation_id,unique_ticket_id,paid,amount)
        if method=="POST":
            recipient = event_registration.reg_email
            send_email_with_attachement(html_content,subject,SEND_EMAIL,recipient)
        return html_content
    elif mail_type==EmailNotificationEvents.ORGANIZER_USER_CANCELLATION:
        html_content, subject = __organizer_email_notification(event_entity.event_title_name,
                                                               event_registration.reg_first_name,
                                                               event_registration.reg_last_name,
                                                               event_registration.reg_unique_id, paid, mail_type,
                                                               amount,cancellation_id)
        if method == 'POST':
            send_email_with_attachement(html_content, subject, SEND_EMAIL, event_entity.event_json().get("email", ""))
        return html_content
    elif mail_type==EmailNotificationEvents.ORGANIZER_EVENT_DELETION:
        html_content,subject = __organizer_email_notification(event_entity.event_title_name,
                                                               event_registration.reg_first_name,
                                                               event_registration.reg_last_name,
                                                               event_registration.reg_unique_id, paid, mail_type,
                                                               amount,cancellation_id)
        if method == 'POST':
            send_email_with_attachement(html_content, subject, SEND_EMAIL, event_entity.event_json().get("email", ""))
        return html_content




def __user_registration_email_notification(quantity,event_title_name,unique_ticket_id):
    subject             = f"You all set your confirmation code is {unique_ticket_id}"
    if quantity > 1:
        reservation_word = "reservation's"
    else:
        reservation_word = "reservation"
    data                 =  {"reservation_word":reservation_word,"title":event_title_name,
                            "quantity":quantity,"subject":subject}
    user_email_template  = env.get_template('user_registration.html')
    result               = user_email_template.render(**data)
    return result,subject

def __user_cancellation_email_notification(event_title,cancellation_id,ticket_unique_id,paid,amount=0):
    subject             = f"Event cancellation confirmation {cancellation_id}"
    user_cancellation_template = env.get_template('user_cancellation.html')
    data = {"event_title":event_title,"ticket_unique_id":ticket_unique_id,"paid":paid,"amount":amount,"subject":subject}
    result                     = user_cancellation_template.render(**data)
    return result,subject

def __organizer_email_notification(event_title,first_name,last_name,ticket_unique_id,paid,mail_type,amount=0,cancellation_id=""):
    data = {"first_name": first_name, "last_name": last_name, "event_title": event_title,
            "ticket_unique_id": ticket_unique_id, "paid": paid, "amount": amount}
    if mail_type==EmailNotificationEvents.ORGANIZER_USER_REGISTRATION:
        subject = f"Event registration confirmation {event_title}"
        organizer_email_template = env.get_template('organizer_user_event_registration.html')
        result = organizer_email_template.render(subject=subject,**data)
        return result,subject
    elif mail_type==EmailNotificationEvents.ORGANIZER_USER_CANCELLATION:
        subject = f"{event_title} has been cancelled,with a confirmation ID: {cancellation_id}"
        organizer_email_template = env.get_template('organizer_user_event_cancellation.html')
        result = organizer_email_template.render(subject=subject,cancellation_id=cancellation_id, **data)
        return result, subject
    elif mail_type==EmailNotificationEvents.ORGANIZER_EVENT_DELETION:
        subject = f"Event cancellation for {event_title}"
        organizer_email_template = env.get_template("organizer_user_event_deletion.html")
        result  = organizer_email_template.render(subject=subject,cancellation_id=cancellation_id,**data)
        return result,subject



def send_user_registration(event_entity:Event,event_registration:Event_Registration,unique_ticket_id):
    ticket_data = __build_ticketing_data(event_entity, event_registration)
    ticket = generate_ticket_html_pdf(ticket_data, unique_ticket_id)
    ticket_path = upload_ticket_to_firebase(ticket["ticket_html"], ticket)
    if event_registration.reg_email:
        response = send_email_with_attachement(html_content, subject, SEND_EMAIL, event_registration.reg_email,
                                               ticket_path, "application/pdf")
        return response
    return False



@profiler
def generate_event_report(session):
    event_reports = ("event_id","event_json")
    query         = "select e.event_id_firebase,e.event_json,timestampdiff(HOUR,sysdate(), e.event_end_time) as diff_hours from event as e having diff_hours>-36 and diff_hours<-20"
    results       = execute_raw_select_query(session,query,event_reports)
    for result in results:
        try:
            event_data = convert_string_to_json(result.get('event_json'))
            if not event_data.get('email') or not 'email' in event_data:
                primary_email = find_email_by_uid(event_data.get('uid'))
                generate_report({'event_id': result.get('event_id'),'user_id':event_data.get('uid'),'send_email':primary_email},session)
            else:
                generate_report({'event_id': result.get('event_id'),'user_id':event_data.get('uid'),'send_email':event_data.get('email')},session)
            logging.info(f"Successfully generated report for {result.get('event_id')}")
        except:
            logging.error(f"Failed to execute the event report {result.get('event_id')}")
            continue


def convert_string_to_json(json_string):
    try:
        event_str_json = json_string.replace("'", '"')
        data = json.loads(event_str_json)
        return data
    except Exception as ex:
        return dict()


def __build_ticketing_data(event_entity,event_registration):
    data = dict()
    data["title"]       = event_entity.event_title_name
    data["quantity"]    = event_registration.quantity
    data["first_name"]  = event_registration.reg_first_name
    data["last_name"]   = event_registration.reg_last_name
    data["ticket_type"] = event_registration.event_ticket.ticket_type
    data["time"]        = event_entity.event_start_time.strftime("%b-%d-%Y").replace("-"," ")
    data["place"]       = event_entity.event_json().get("address")
    if event_registration.event_payment:
        data["amount"]  = event_registration.event_payment.paid_amount
    data["paid"]        = event_registration.event_ticket.ticket_type==PaymentType.PAID
    return data