import os
import traceback
import stripe
import uuid
from stripe.error import InvalidRequestError,AuthenticationError
from kilimanjaro_service.db.stripe_model import *
from kilimanjaro_service.db import DbRepo
from kilimanjaro_service.service.utils_handler import *


class StripeTransactionEnum:
    CREATE_CUSTOMER = "CREATE_CUSTOMER"
    CREATE_CHARGE   = "CREATE_CHARGE"
    CREATE_REFUND   = "CREATE_REFUND"

class StripeStatusEnum:
    FAILED      = "FAILED"
    SUCCESS     = "SUCCESS"


class StripeInvalidRefundException(Exception):
    def __init__(self):
        super().__init__()

stripe.api_key = os.getenv("STRIPE_KEY")

def __stripe_process_charge(data,source,destination):
    result = dict()
    transaction_id = str(uuid.uuid4())
    try:
        result["transaction_id"] = transaction_id
        result["request"]        = data
        result["charge_type"]    = StripeTransactionEnum.CREATE_CHARGE
        result["source"]         = source
        result["destination"]    = destination
        response                 = stripe.Charge.create(**data,idempotency_key=transaction_id)
        result["response"]       = response
        result["status"]         = StripeStatusEnum.SUCCESS
        result["unique_id"]      = response["id"]
    except InvalidRequestError as ex:
        result["response"]  = ex.json_body
        result["status"]    = StripeStatusEnum.FAILED
        result["unique_id"] = ex.request_id
    except Exception as ex:
        error_string = traceback.format_exc()
        result["response"]  = f"Failed due to some server issue {str(error_string)}"
        result["status"]    = StripeStatusEnum.FAILED
        result["unique_id"] = str(uuid.uuid4())
    finally:
        return result


def __stripe_process_refund(transaction_id,unique_id,source,destination):
    result = dict()
    try:
        result["request"] = {"charge":unique_id}
        result["charge_type"] = StripeTransactionEnum.CREATE_REFUND
        result["source"] = source
        result["destination"] = destination
        result['transaction_id'] = transaction_id
        response = stripe.Refund.create(charge = unique_id)
        result["response"] = response
        result["status"] = StripeStatusEnum.SUCCESS
        result["unique_id"] = response["id"]
    except InvalidRequestError as ex:
        result["response"] = ex.json_body
        result["status"] = StripeStatusEnum.FAILED
        result["unique_id"] = ex.request_id
    except AuthenticationError as ex:
        result["response"]=ex.json_body
        result["status"]  = StripeStatusEnum.FAILED
        result["unique_id"]=ex.request_id
    except Exception as ex:
        error_string = traceback.format_exc()
        if not "transaction_id" in result:
            result["transaction_id"]="Transaction Id does not exist"
        result["response"] = f"Failed due to some server issue {str(error_string)}"
        result["status"] = StripeStatusEnum.FAILED
        result["unique_id"] = str(uuid.uuid4())
    finally:
        return result


@profiler
def stripe_charge_transaction(data,dbSession):
    stripe_request     = data["stripe_request"]
    source             = data["source"]
    customer_uid       = data["uid"]
    customer_stripe_id = data["stripe_customer_id"]
    destination        = data["destination"]
    response_meta      = __stripe_process_charge(stripe_request,source,destination)
    stripe_customer    = dbSession.query(StripeCustomer).\
        filter(and_(StripeCustomer.customer_unq_id==customer_uid,StripeCustomer.stripe_customer_id==customer_stripe_id)).one()
    stripe_transaction = StripeTransaction(**response_meta,customer_stripe_id=customer_stripe_id,stripe_customer=stripe_customer)
    response_meta["amount"]=data["amount"]
    response_meta["uid"]=data["uid"]
    dbSession.add(stripe_transaction)
    return response_meta





@profiler
def stripe_refund_transaction(data,dbSession):
    stripe_customer      = dbSession.query(StripeCustomer).filter(and_(StripeCustomer.customer_unq_id==data["uid"],StripeCustomer.stripe_customer_id==data["stripe_customer_id"])).one() # Check if the customer exist
    #Check the latest charge transaction
    stripe_transaction   = dbSession.query(StripeTransaction).filter(and_(StripeTransaction.customer_stripe_id==stripe_customer.pk_id,StripeTransaction.transaction_id==data["transaction_id"],StripeTransaction.charge_type==StripeTransactionEnum.CREATE_CHARGE))\
        .order_by(asc(StripeTransaction.time_executed)).first()
    #Check if the transaction has already performed a refund
    check_strip_refund   = DbRepo(dbSession,StripeTransaction).execute_query_for_one(transaction_id=stripe_transaction.transaction_id,charge_type=StripeTransactionEnum.CREATE_REFUND,status=StripeStatusEnum.SUCCESS)
    if check_strip_refund:
        raise StripeInvalidRefundException()
    #Execute refund
    result = __stripe_process_refund(data['transaction_id'],stripe_transaction.unique_id,stripe_transaction.source,stripe_transaction.destination)
    #Persist the stripe transaction
    stripe_transaction = StripeTransaction(**result,customer_stripe_id=data['uid'],stripe_customer=stripe_customer)
    result["amount"]=data["amount"]
    dbSession.add(stripe_transaction)
    return result


@profiler
def register_stripe_customer(data,dbSession):
    stripe_customer = StripeCustomer(data["customer_id"],data["uid"])
    dbSession.add(stripe_customer)


