
class CatchAllEvent:
    EVENT_CREATION      = "event_creation"
    EVENT_UPDATED       = "event_updated"
    EVENT_DELETED       = "event_deleted"
    EVENT_CANCELLED     = "event_cancelled"
    EVENT_REGISTRATION  = "event_registration"


class EmailTrigger:
    EVENT_FAILED          = "event_failed"
    EVENT_FAILED_REFUND   = "event_failed_refund"


def catch_all_emails(event,data:dict):
    if event==CatchAllEvent.EVENT_CREATION:
        html = f"<p> The organizer with the ID: <b>{data['event_organizer']}</b> has created an event with title {data['event_title']}\n" \
               f"Event details in {data['event_url']}" \
               "</p>"
        subject = f"EVENT CREATED EVENT ID: {data['event_id']}"
        return (subject,html)
    elif event==CatchAllEvent.EVENT_UPDATED:
        html = f"<p> The organizer with the ID: <b>{data['event_organizer']}</b> has updated the event with event id {data['event_id']}\n" \
               f"Event details in {data['event_url']}" \
               "</p>"
        subject = f"EVENT UPDATED EVENT ID: {data['event_id']}"
        return (subject, html)
    elif event==CatchAllEvent.EVENT_DELETED:
        if not data['paid']:
            html = f"<p> The organizer with the ID: <b>{data['event_organizer']}</b> has deleted the event with event id {data['event_id']}\n" \
                   f"Confirmation number {data['confirmation']}" \
                   "</p>"
            subject = f"EVENT DELETED EVENT ID: {data['event_id']}"
            return (subject, html)
        else:
            html = f"<p> The organizer with the ID: <b>{data['event_organizer']}</b> has deleted the event with event id {data['event_id']}, All payments has been has been refunded.\n" \
                   f"Confirmation number {data['confirmation']}" \
                   "</p>"
            subject = f"EVENT DELETED EVENT ID: {data['event_id']}"
            return (subject, html)

    elif event==CatchAllEvent.EVENT_CANCELLED:
        if not data['paid']:
            html = f"<p> The attendee with the ID: <b>{data['attendee_id']}</b> has cancelled there attendance for {data['event_title']} \n" \
                   f"Confirmation number {data['confirmation']}" \
                   "</p>"
            subject = f"EVENT DELETED EVENT ID: {data['event_id']}"
            return (subject,html)
        else:
            html = f"<p> The attendee with the ID: <b>{data['attendee_id']}</b> has cancelled there attendance for {data['event_title']}, {data['amount']} USD has been refunded.\n" \
                   f"Confirmation number {data['confirmation']}" \
                   "</p>"
            subject = f"EVENT DELETED EVENT ID: {data['event_id']}"
            return (subject, html)
    elif event==CatchAllEvent.EVENT_REGISTRATION:
        if not data['paid']:
            html = f"<p> The attendee with the ID: <b>{data['attendee_id']}</b> has booked the event <b> {data['event_title']} </b>, with event id {data['event_id']} \n" \
                   f"Ticket Number {data['unique_id']}, Ticket Quantity {data['quantity']}" \
                   "</p>"
            subject = f"New Ticket ID: {data['unique_id']}"
            return (subject,html)
        else:
            html = f"<p> The attendee with the ID: <b>{data['attendee_id']}</b> has booked the event <b> {data['event_title']} </b>, with event id {data['event_id']} and has made a payment of  {data['amount']} USD \n" \
                   f"Ticket Number {data['unique_id']}, Ticket Quantity {data['quantity']}" \
                   "</p>"
            subject = f"New Ticket ID: {data['unique_id']}"
            return (subject, html)