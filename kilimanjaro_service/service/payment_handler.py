from kilimanjaro_service.service import *
from kilimanjaro_service.service.stripe_service import stripe_refund_transaction,stripe_charge_transaction,StripeStatusEnum
from kilimanjaro_service.db import *
from kilimanjaro_service.db.model import *
from kilimanjaro_service.service.utils_handler import profiler
from datetime import datetime
import logging


class FailedTransaction(Exception):
    pass

class UnableToProcessPayment(Exception):
    pass

@profiler
def process_event_payment(event_registration):
    '''
    Payment process handler
    :param event_registration:
    :return: payment_payload
    '''
    try:
        event_data = {"stripe_request":event_registration.get("stripe_request"),
                      "uid":event_registration.get("uid"),"destination":event_registration.get("destination"),
                      "amount":event_registration.get("amount"),"source":event_registration.get("source"),"stripe_customer_id":event_registration.get("stripe_customer_id")}
        with session_scope() as session:
            payment_data = stripe_charge_transaction(event_data,session)
        if payment_data['status'] == StripeStatusEnum.FAILED:
            raise UnableToProcessPayment()
        current_date = datetime.now().timestamp()*1000
        event_registration_data                      = event_registration.get("registration")
        ticket_id                                    = event_registration_data.get("ticket_id")
        primay_email                                 = event_registration_data.get("registrations")[0]["email"]
        event_registration_data["amount"]            = event_registration.get("amount")
        event_registration_data["transaction_id"]    = payment_data.get("transaction_id")
        event_registration_data["customer_id"]       = event_registration.get("stripe_customer_id")
        event_registration_data["charge_id"]         = payment_data.get("unique_id")
        event_registration_data["payment_date_time"]         = current_date
        try:
            with session_scope() as session:
             registration =   register_user_for_event(event_registration_data,session)
        except Exception as ex:
            #Refund logic first notify the user, refund the funds and send the refund email.
            with session_scope() as session:
                event = find_event_by_ticket_firebase_id(ticket_id,session)
                send_attendee_failed_transaction({'title':event.event_title_name,'email':primay_email})
            stripe_customer_id = event_registration.get("stripe_customer_id")
            refund_data = {"uid": event_registration.get("uid"), "stripe_customer_id": stripe_customer_id,
                           "unique_id": payment_data.get("unique_id"),"transaction_id":payment_data.get("transaction_id"),"amount":event_data.get("amount")}
            with session_scope() as session:
                result = stripe_refund_transaction(refund_data,session)
            send_failed_refund_email({"amount":event_registration_data.get("amount"),"confirmation_number":result.get('transaction_id')})
            raise FailedTransaction()
        return {"event_id":registration.get("event_id"),"ticket_unique_id":registration.get("unique_id"),"user_id":registration.get("u_id")}
    except Exception as ex:
        logging.exception("message")
        logging.error("Transaction failed")
        raise ex