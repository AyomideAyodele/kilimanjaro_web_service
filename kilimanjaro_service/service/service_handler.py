import os
import base64
import logging
import requests

from kilimanjaro_service.service.utils_handler import *
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import *
from main import kilimanjaro_db,storage,firebase_handler
from jinja2 import Environment, FileSystemLoader


DOWNLOAD_PDF_URL = "https://us-central1-kilimanjaro-355fa.cloudfunctions.net/pdfGenerator"
DOWNLOAD_CSV_URL = "https://us-central1-kilimanjaro-355fa.cloudfunctions.net/pdfGenerator"
send_grid_mail = SendGridAPIClient(os.environ.get("SEND_GRID_API"))
SEND_EMAIL     = "noreply@thekilimanjaroapp.com"
file_loader = FileSystemLoader('')
env = Environment(loader=file_loader)
storage_client = storage.storage.Client()
bucket_blob = storage_client.get_bucket(os.environ.get("BUCKET_NAME"))
TICKET_GENERATION = "generated_ticket"

class PaymentType:
    FREE        = "FREE"
    PAID        = "PAID"
    DONATION    = "DONATION"

class EventStatus:
    ATTENDING       = "ATTENDING"
    CANCELLED       = "CANCELLED"


class PaymentStatus:
    COMPLETED = "COMPLETED"
    PENDING   = "PENDING"
    REFUNDED  = "REFUNDED"

class RefundStatus:
    COMPLETED = "COMPLETED"
    PENDING = "PENDING"


@profiler
def generate_event_uid(event_name,count):
    '''
    :TODO to be worked to be extensive and robust
    :param event_name:
    :param count:
    :return:
    '''
    event_name = event_name.strip()
    e_name     = event_name.upper().split(" ")
    g_name      = "".join([e[0] for e in e_name])
    hash_name    = hash(event_name)%1000_000
    generated_name = f'{g_name}-{hash_name}-{count}'
    return generated_name

@profiler
def generate_cancellation_id(ticket_id):
    """
    :TODO TO BE WORKED ON EXTENSIVELY
    :param ticket_id:
    :return:
    """
    postfix = hash(ticket_id)%1000_000
    prefix = ticket_id.split("-")[0]
    suffix = ticket_id.split("-")[1]
    generated_id = f"{prefix}-{suffix}-{postfix}"
    return generated_id

@profiler
def generate_event_cancellation_id(event_id,event_title):
    event_title = event_title.strip()
    e_name = event_title.upper().split(" ")
    g_name = "".join([e[0] for e in e_name])
    event_id_hash = hash(event_id)%1000_000
    timestamp     = int(datetime.now().timestamp())%1000_000
    return f"{g_name}-{event_id_hash}-{timestamp}"

@profiler
def generate_ticket_pdf(registration_data):
    event = registration_data[0]
    data = dict()
    template = env.get_template(f"ticket.html")
    res = template.render(title=event['title'], quantity=event['quantity'], ticket_id=event['unique_id'],
                          first_name=event['first_name'], ticket_type=event['ticket_type'],
                          last_name=event['last_name'],
                          time=event['date_time'], place=event['place'], amount=event.get('amount', 0.0),
                          paid=event.get('paid'))
    #get_pdf_document(res,dir_path)
    data['ticket_name'] = f'{event["unique_id"]}.pdf'
    data['ticket_html']=res
    data.update(event)
    return data


@profiler
def generate_ticket_html_pdf(data,unique_id):
    result                = dict()
    template              = env.get_template(f"ticket.html")
    res                   = template.render(**data)
    result['ticket_name'] = f'{unique_id}.pdf'
    result['ticket_html'] = res
    return result


@profiler
def get_email_html(html_dir,**kwargs):
    template = env.get_template(html_dir)
    result = template.render(**kwargs)
    return result

@profiler
def get_pdf_document(html,file_path):
    html = html.encode("utf-8")
    response = requests.post(DOWNLOAD_PDF_URL, data=html, headers={"Content-Type": "text/plain"})
    if response.status_code == 200:
        with open(file_path, 'wb') as file_obj:
            file_obj.write(response.content)
        return file_path
    raise UnableToDownloadPDF()



@profiler
def send_ticket_email(ticket_pdf_html,data):
    try:
        attach = upload_ticket_to_firebase(ticket_pdf_html,data)
        if data["quantity"]>1:
            reservation = "reservation's"
        else:
            reservation = "reservation"
        html_content = f'<p> Your confirmation has been sent to {data["organizer_email"]} for {data["quantity"]} {reservation} of {data["title"]} '
        f'ticket, attached is your ticket for the {reservation} <br/> Thank you,<br/> The Kilimanjaro App Team. </p>'
        res = send_email_with_attachement(html_content,f"You are all set for {data['title']}",from_email=SEND_EMAIL,to_email=data['to_email'],attachment=attach,attachement_type="application/pdf")
        return res
    except Exception as ex:
        print(ex)
        raise ex


@profiler
def notify_organizer_email(data):
    if not "organizer_email" in data or not data.get("organizer_email"):
        return False
    try:
        logging.info("Notifying Oragnizer a User just registered for an event")
        if data["quantity"]>1:
            subject = f"{data['quantity']} ticket's has been booked for {data['title']}"
        else:
            subject = f"{data['quantity']} ticket has been booked for {data['title']}"
        if not data.get("paid"):
            html_content = f'<p> Your event {data["title"]} has been booked by {data["first_name"]} {data["last_name"]} with a ticket id of  {data["unique_id"]}  <br/>Thank you,<br/>The Kilimanjaro App Team.</p>'
        else:
            html_content = f'<p> Your event {data["title"]} has been booked by {data["first_name"]} {data["last_name"]} with a ticket id of  {data["unique_id"]} and has made a payment of {data["amount"]} <br/>Thank you,<br/>The Kilimanjaro App Team.</p>'
        res = send_email_with_attachement(html_content,subject,from_email=SEND_EMAIL,to_email=data['organizer_email'])
        return res
    except Exception as ex:
        raise ex

@profiler
def get_ticket_document(user_id,event_id,unique_id):
    document_name = kilimanjaro_db.child(TICKET_GENERATION).child(user_id).child(event_id).child(unique_id).get()
    if not document_name:
        raise DocumentNotFound()
    return document_name

@profiler
def document_ticket_pdf(ticket_name):
    data = bucket_blob.get_blob(ticket_name)
    return data.public_url

@profiler
def upload_ticket_to_firebase(html, ticket_info:dict):
    html = html.encode("utf-8")
    response = requests.post(DOWNLOAD_PDF_URL, data=html, headers={"Content-Type": "text/plain"})
    if not response.status_code == 200:
        raise UnableToDownloadPDF()
    save_ticket = kilimanjaro_db.child("generated_ticket")
    data_path   = f"tickets/{ticket_info['ticket_name']}"
    ticket_info["data_path"]=data_path
    ticket_blob = bucket_blob.blob(f"{ticket_info['data_path']}")
    ticket_blob.upload_from_string(response.content, content_type="application/pdf")
    save_ticket.child(ticket_info['u_id']).child(ticket_info['event_id']).child(ticket_info['unique_id']).set(
        f'tickets/{ticket_info["ticket_name"]}')
    return ticket_info["data_path"]

@profiler
def create_csv_report(rows,column):
    result = ",".join(column)
    for row in rows:
        result+="\n"
        row   = [ str(r) if r else ""  for r in row ]
        result+=",".join(row)
    return result


@profiler
def upload_document(data):
    save_document = kilimanjaro_db.child("documents")
    document_blob = bucket_blob.blob(f'{data["document_path"]}')
    document_blob.upload_from_string(data['csv_datas'],content_type="text/csv")
    save_document.child(data["user_id"]).child(data["event_id"]).\
        child(data["transaction_id"]).set(data["document_path"])
    return data["document_path"]





@profiler
def send_email_with_attachement(content,subject,from_email,to_email,attachment=None,attachement_type=None):
    message = Mail(from_email=from_email,to_emails=to_email,subject=subject,html_content=content)
    if attachment:
        message.attachment = _create_attachement(attachment,attachement_type)
    message = message.get()
    response = send_grid_mail.send(message)
    if response.status_code==202:
        return True
    else:
        return False

def send_cancellation_by_user_email_to_organizer(data, cancellation_id, paid=False):
    if not "organizer_email" in data or data.get("organizer_email") is None:
        logging.info("Skipping the organizer email, because it not present ")
        return
    if paid:
        html = f'<p> {data["first_name"]} {data["last_name"]} has cancelled there event {data["event_title"]} with ticket id :{data["ticket_unique_id"]}.\n' \
               f'{data["refund_amount"]} USD will be refunded and they will also be getting a cancellation email \n Thank you, \n The Kilimanjaro App Team </p>'
        send_email_with_attachement(html,f"Event cancellation for {data['event_title']} : {cancellation_id}",from_email=SEND_EMAIL,to_email=data["organizer_email"])
    else:
        html = f'<p> {data["first_name"]} {data["last_name"]} has cancelled there event {data["event_title"]} with ticket id :{data["ticket_unique_id"]}.\n' \
               f'They will also be getting a cancellation email \n Thank you, \n The Kilimanjaro App Team </p>'
        send_email_with_attachement(html, f"Event cancellation for {data['event_title']} : {cancellation_id}",
                                    from_email=SEND_EMAIL, to_email=data["organizer_email"])


def send_cancellation_email_to_attendees(data,cancellation_id,paid=False):
    if not "attendee_email" in data or data.get("attendee_email") is None:
        logging.info("No Attendee email, therefore skipping the attendee email")
        return
    if paid:
        html = f'<p> Your event {data["event_title"]} with ticket id :{data["ticket_unique_id"]} has been cancelled.\n' \
               f'{data["refund_amount"]} USD will be refunded <br/> Thank you, <br/> The Kilimanjaro App Team </p>'
        send_email_with_attachement(html, f"Cancellation confirmation {cancellation_id}",
                                    from_email=SEND_EMAIL, to_email=data["attendee_email"])
    else:
        html = f'Your event {data["event_title"]} with ticket id :{data["ticket_unique_id"]} has been cancelled.\n' \
               f'Thank you, \n The Kilimanjaro App Team </p>'
        send_email_with_attachement(html, f"Cancellation confirmation {cancellation_id}",
                                    from_email=SEND_EMAIL, to_email=data["attendee_email"])

def send_organizer_event_deletion_email(data, cancellation_id, paid=False):
    if not "organizer_email" is data or data.get("organizer_email") is None:
        logging.info("Skipping the organizer email, because it not present ")
        return
    if paid:
        html = f"<p> You have cancelled the event {data['event_title']}. We will notify the attendees and also refund the payments made.\n" \
               f"The confirmation number is {cancellation_id},\n Thank you,\n The Kilimanjaro App Team </p>"
        send_email_with_attachement(html,f"Event Cancellation {cancellation_id}",
                                    from_email=SEND_EMAIL, to_email=data["organizer_email"])
    else:
        html = f"<p> You have cancelled the event {data['event_title']}. We will notify the attendees.\n" \
               f"The confirmation number is {cancellation_id},\n Thank you,\n The Kilimanjaro App Team </p>"
        send_email_with_attachement(html,f"Event Cancellation {cancellation_id}",
                                    from_email=SEND_EMAIL, to_email=data["organizer_email"])

def send_attendee_failed_transaction(data):
    html = f"<p> We are not able to book your {data['event_title']} at this time due to some errors that occurred. If you have made any payments you will receive a confirmation email \n" \
               f"Thank you,\n The Kilimanjaro App Team </p>"
    send_email_with_attachement(html,f"FAILED TO BOOK EVENT",from_email=SEND_EMAIL,to_email=data['email'])

def send_failed_refund_email(data):
    html = f"<p> Your refund have been completed for an amount of  {data['amount']} USD, with a confirmation number of {data['confirmation_number']} \n" \
           f"Thank you,\n The Kilimanjaro App Team </p>"
    send_email_with_attachement(html, f"REFUND HAS BEEN SUCCESSFULLY COMPLETED: {data['confirmation_number']} ", from_email=SEND_EMAIL,
                                to_email=data['email'])
@profiler
def firebase_update_event(event_id,kwargs):
    if not kwargs:
        return {}
    result_ref = firebase_find_event(event_id)
    result = result_ref.get()
    if result:
        kwargs['update']=str(False)
        result.update(kwargs)
        result_ref.update(result)
        return result

@profiler
def firebase_event_orders(event_id):
    result_ref = kilimanjaro_db.child("orders").child(event_id)
    return result_ref


@profiler
def find_email_by_uid(uid):
    email_result = kilimanjaro_db.child("profiles").child(uid).get()
    return email_result.get("email")


@profiler
def cancel_event_firebase(event_id,charge_id,paid):
    result_ref = kilimanjaro_db.child("orders").child(event_id).child(charge_id)
    result = result_ref.get()
    if not result:
        raise NotValidEventException()
    if paid:
        update = {"action":"refund"}
        result_ref.update(update)


@profiler
def firebase_find_event(event_id):
    result = kilimanjaro_db.child("events").child("approved").child(event_id)
    return result


@profiler
def firebase_user_name(event_id):
    result = kilimanjaro_db.child('profiles').child(event_id).get()
    return result

@profiler
def _create_attachement(file_name,file_type):
    data =  bucket_blob.get_blob(file_name)
    data = data.download_as_string()
    encoded = base64.b64encode(data).decode()
    attach = Attachment()
    attach.file_content = FileContent(encoded)
    attach.file_type    = FileType(file_type)
    attach.file_name    = FileName(file_name)
    return attach
