from kilimanjaro_service.db.model import *
from kilimanjaro_service.service.stripe_service import stripe_refund_transaction
from kilimanjaro_service.service.service_handler import *
from kilimanjaro_service.service.catch_all_email import *
from datetime  import datetime

NOTIFICATION_EMAIL = "support@thekilimanjaroapp.com"
EVENT_VIEW_URL     = "http://kilimanjaro-355fa.appspot.com/kilimanjaro/event/{}"

EMAIL_TRIGGERS = {
    EmailTrigger.EVENT_FAILED_REFUND:send_failed_refund_email,
    EmailTrigger.EVENT_FAILED       :send_attendee_failed_transaction
}




@profiler
def create_service_event(data,session):
    if "description" in data:
        data["description"]=data["description"].replace("'","`")
    if "title" in data:
        data["title"]=data["title"].replace("'","`")
    create_event(data,session)
    firebase_update_event(data["event_id"],data)

def email_trigger(event,data):
    """
    :TODO Add exception handler
    :param event:
    :param data:
    :return:
    """
    func = EMAIL_TRIGGERS.get(event)
    func(data)

@profiler
def deregister_event(data,session):
    event_registration = DbRepo(session, Event_Registration). \
        execute_query_for_one(reg_unique_id=data['ticket_unique_id'], user_firebase_id=data['user_id'],
                              event_status=EventStatus.ATTENDING)
    event = DbRepo(session, Event).execute_query_for_one(event_id_firebase=data["event_id"])
    de_register_data = __deregistration_handler(event,event_registration,session)
    de_register_data['refund'] = check_valid_for_refund(de_register_data,event)
    if de_register_data["refund"]:
        send_cancellation_email_to_attendees(de_register_data, de_register_data["cancellation_id"], True)
        send_cancellation_by_user_email_to_organizer(de_register_data, de_register_data["cancellation_id"], True)
        catch_all_transaction(CatchAllEvent.EVENT_CANCELLED, attendee_id=data["user_id"], event_id=data['event_id'],
                              event_title=de_register_data["event_title"], confirmation=de_register_data["cancellation_id"],
                              amount=de_register_data["amount"],
                              paid=True)
    else:
        send_cancellation_email_to_attendees(de_register_data, de_register_data["cancellation_id"], False)
        send_cancellation_by_user_email_to_organizer(de_register_data, de_register_data["cancellation_id"], False)
        catch_all_transaction(CatchAllEvent.EVENT_CANCELLED, attendee_id=data["user_id"], event_id=data['event_id'],
                              event_title=de_register_data["event_title"],
                              confirmation=de_register_data["cancellation_id"],
                              paid=False)





def __deregistration_handler(event:Event,event_registration:Event_Registration,session):
    if not event_registration and not event:
        raise ResultNotFound()
    event_ticket = event_registration.event_ticket
    event_org_email = event.event_json().get("email")
    event_registration.event_status = EventStatus.CANCELLED
    cancellation_id = generate_cancellation_id(event_registration.reg_unique_id)
    event_registration.event_cancellation_id = cancellation_id
    event_registration.event_cancellation_datetime = datetime.now()
    event_ticket.ticket_sold = event_ticket.ticket_sold - event_registration.quantity
    if event_registration.event_payment_id:
        event_payment = event_registration.event_payment
        refund_data = dict()
        event_payment.status = PaymentStatus.REFUNDED
        refund_data["refund_amount"] = event_payment.paid_amount
        refund_data["event_amount"] = event_payment.paid_amount
        refund_request = {'uid':event_registration.user_firebase_id,'stripe_customer_id':event_payment.stripe_customer_id,
                          'transaction_id':event_payment.confirmation_number,'amount':event_payment.paid_amount}
        refund_response = stripe_refund_transaction(refund_request,session)
        refund_data['unique_id'] = refund_response['unique_id']
        event_refund = refund_event_ticket(refund_data, session)
        event_registration.event_refund = event_refund
        attendee_email_data = {"ticket_unique_id": event_registration.reg_unique_id,"cancellation_id":cancellation_id,
                               "refund_amount": event_payment.paid_amount, "event_title": event.event_title_name,
                               "attendee_email": event_registration.reg_email,"first_name": event_registration.reg_first_name,
                               "amount":event_payment.paid_amount,"last_name": event_registration.reg_last_name,
                               "organizer_email": event_org_email,"paid":True}
        return attendee_email_data
    else:
        attendee_email_data = {"ticket_unique_id": event_registration.reg_unique_id,
                               "cancellation_id": cancellation_id,
                               "event_title": event.event_title_name,
                               "attendee_email": event_registration.reg_email,
                               "first_name":event_registration.reg_first_name,
                                "last_name":event_registration.reg_last_name,
                                "organizer_email": event_org_email,"paid":False}
        return attendee_email_data
def catch_all_transaction(event,**kwargs):
    subject,content = catch_all_emails(event,kwargs)
    send_email_with_attachement(content,subject,from_email="noreply@thekilimanjaroapp.com",to_email=NOTIFICATION_EMAIL)

@profiler
def get_user_tickets(user_id,session):
    registrations = get_users_registration(user_id,session)
    result=[]
    for register in registrations:
        data=dict()
        ticket = register.event_ticket
        event = ticket.event
        if not event.valid_event:
            continue
        event_blob = event.event_json()
        data["address"]=event_blob["address"]
        data["quantity"]=register.quantity
        if "phone" in event_blob:
            data["phone_number"]=event_blob["phone"]
        data["event_id"]=event.event_id_firebase
        data["image"]=event.event_main_image
        data["title"]=event.event_title_name
        data["start_date"]=event.event_start_time
        data["end_date"]=event.event_end_time
        data["ticket_unique_id"]=register.reg_unique_id
        data["ticket_title"]=ticket.ticket_title
        data["ticket_type"] = ticket.ticket_type
        data["description"] = event_blob.get("description")
        if not ticket.ticket_type == PaymentType.FREE:
            data["price"] = str(ticket.ticket_price)
        data["ticket_creation"] = ticket.ticket_datetime;
        data["ticket_id"]  = ticket.ticket_id_firebase
        data["ticket_pdf"]=get_user_ticket_document(user_id, event.event_id_firebase, register.reg_unique_id)
        result.append(data)
    return result


@profiler
def cancel_event_registrations(data, session):
    user_event = get_user_event_relation(data["event_id"],data["user_id"],session)
    user_event = user_event if user_event and user_event.valid_event else None
    event = get_event_by_id(user_event.event_unq_id,session)
    event = event if event and event.valid_event else None
    if user_event is None and event is None:
        raise NotValidEventException()
    event.valid_event = False
    event.valid_event = False
    event_json  = event.event_json()
    organizer_email = event_json.get("email")
    paid_event = False
    event_json['valid_event']=False
    event_cancellation_id = generate_event_cancellation_id(event.event_id_firebase,event.event_title_name)
    event.event_cancellation_id =event_cancellation_id
    for ticket in event.event_tickets:
        for registration in ticket.event_registration:
            de_register_data = __deregistration_handler(event,registration,session)
            if de_register_data["paid"]:
                send_cancellation_email_to_attendees(de_register_data, de_register_data["cancellation_id"], True)
                catch_all_transaction(CatchAllEvent.EVENT_CANCELLED, attendee_id=data["user_id"],
                                      event_id=data['event_id'],
                                      event_title=de_register_data["event_title"],
                                      confirmation=de_register_data["cancellation_id"],
                                      amount=de_register_data["amount"],
                                      paid=True)

                paid_event = True
            else:
                send_cancellation_email_to_attendees(de_register_data, de_register_data["cancellation_id"], False)
                catch_all_transaction(CatchAllEvent.EVENT_CANCELLED, attendee_id=data["user_id"],
                                      event_id=data['event_id'],
                                      event_title=de_register_data["event_title"],
                                      confirmation=de_register_data["cancellation_id"],
                                      paid=False)

    ref = firebase_find_event(event.event_id_firebase)
    ref.delete()
    send_organizer_event_deletion_email({'event_title':event.event_title_name, 'organizer_email':organizer_email}, event_cancellation_id, paid_event)
    catch_all_transaction(CatchAllEvent.EVENT_DELETED,event_organizer=event_json.get("uid"),event_id=event.event_id_firebase,confirmation=event_cancellation_id,paid=paid_event)

@profiler
def search_pagination(data,session):
    page,size      = data["page"],data["size"]+1
    result         =  search_for_event(data["search_word"],page,size,session)
    result         = [event.event_json() for event in result]
    final_result       = []
    for event in result:
        current_date    = datetime.now().timestamp()
        __check_bookable(current_date,event)
        final_result.append(event)
    return final_result

@profiler
def register_user_for_event(data,session):
    event_registrations = create_event_registration(data,session)
    user_ticket        = generate_ticket_pdf(event_registrations)
    send_ticket_email(user_ticket['ticket_html'],user_ticket)
    notify_organizer_email(user_ticket)
    if user_ticket['paid']:
        catch_all_transaction(CatchAllEvent.EVENT_REGISTRATION,attendee_id=user_ticket['u_id'],event_title=user_ticket['title'],
                          event_id=user_ticket['event_id'],unique_id=user_ticket['unique_id'],paid=True,amount=user_ticket['amount'],
                          quantity=user_ticket['quantity'])
    else:
        catch_all_transaction(CatchAllEvent.EVENT_REGISTRATION,attendee_id=user_ticket['u_id'],event_title=user_ticket['title'],
                          event_id=user_ticket['event_id'],unique_id=user_ticket['unique_id'],paid=False,
                          quantity=user_ticket['quantity'])
    return user_ticket

@profiler
def get_user_ticket_document(userid, eventid, unique_id):
    document_name = get_ticket_document(userid,eventid,unique_id)
    document_url  = document_ticket_pdf(document_name)
    return document_url

@profiler
def get_ticket(unique_id,user_id,session):
    event_registration = get_ticket_by_id(unique_id,session)
    ticket = event_registration.event_ticket
    event = ticket.event
    data = dict()
    event_blob = event.event_json()
    data["address"] = event_blob["address"]
    data["quantity"] = event_registration.quantity
    if "phone" in event_blob:
        data["phone_number"] = event_blob["phone"]
    data["event_id"] = event.event_id_firebase
    data["image"] = event.event_main_image
    data["title"] = event.event_title_name
    data["start_date"] = event.event_start_time
    data["end_date"] = event.event_end_time
    data["ticket_unique_id"] = event_registration.reg_unique_id
    data["ticket_title"] = ticket.ticket_title
    data["ticket_type"]  = ticket.ticket_type
    data["description"]  = event_blob.get("description")
    if not ticket.ticket_type == PaymentType.FREE:
        data["price"] = str(ticket.ticket_price)
    data["ticket_creation"] = ticket.ticket_datetime;
    data["ticket_pdf"] = get_user_ticket_document(user_id, event.event_id_firebase,unique_id)
    return data

@profiler
def find_event_by_id(event_id,session):
    data = get_event_by_id(event_id,session)
    if data :
        result = data.event_json()
        current_date = datetime.now().timestamp()
        __check_bookable(current_date, result)
        return result
    else:
        return dict()


def __check_bookable(current_date, result):
    if not 'end' in result:
        result['bookable'] = False
    else:
        result['end'] = convert_datetime_from_str(result['end']).timestamp() *1000 if isinstance(result['end'], str) else \
        result['end']
        result['start'] = convert_datetime_from_str(result['start']).timestamp()*1000 if isinstance(result['start'],
                                                                                               str) else result['start']
        end_date = result['end'] / 1000
        if current_date < end_date:
            result['bookable'] = True
        else:
            result['bookable'] = False


@profiler
def user_valid_for_ticket(user_id,ticket_id,session):
    ticket        = DbRepo(session,Event_Ticket).execute_query_for_one(ticket_id_firebase=ticket_id)
    if not ticket:
        raise ResultNotFound()
    event = ticket.event
    registrations = DbRepo(session,Event_Registration).execute_query(user_firebase_id=user_id,event_ticket_id=ticket.ticket_pk_id,event_status=EventStatus.ATTENDING)
    count = 0
    for registration in registrations:
        count+=registration.quantity
    if ticket.ticket_type==PaymentType.FREE:
        data = {"valid_for_purchase":ticket.ticket_purchasable>count and event.event_start_time>=datetime.now(),
                "ticket_title":ticket.ticket_title,"type":PaymentType.FREE.lower(),"event_id":event.event_id_firebase,
                "max_purchasable":ticket.ticket_purchasable-count,"ticket_id":ticket_id}
    elif ticket.ticket_type==PaymentType.PAID:
        data = {"valid_for_purchase": ticket.ticket_purchasable>count and event.event_start_time>=datetime.now(),
                "ticket_title": ticket.ticket_title, "type": PaymentType.PAID.lower(),
                "event_id": event.event_id_firebase, "price": str(ticket.ticket_price),
                "max_purchasable": ticket.ticket_purchasable - count, "ticket_id": ticket_id}
    else:
        data = {"valid_for_purchase": ticket.ticket_purchasable>count and event.event_start_time>=datetime.now(),
                "ticket_title": ticket.ticket_title, "type": PaymentType.DONATION.lower(),
                "event_id": event.event_id_firebase, "price":str(ticket.ticket_price),
                "max_purchasable": ticket.ticket_purchasable - count, "ticket_id": ticket_id}
    return data

@profiler
def search_by_suggestion(data,session):
    page,size          = 1,5
    result = search_for_event(data["search_word"],page,size,session)
    result = [ {"event_id":event.event_id_firebase,"event_title":event.event_title_name} for event in result]
    return result

@profiler
def home_page_search(data,session):
    longitude,latitude = data["longitude"],data["latitude"]
    result             = home_page_query(data["page"],data["size"],session,start_time=data["start_time"],end_time=data["end_time"],longitude=longitude,latitude=latitude,radius=data["radius"])
    final_result       = []
    for event in result:
        event = event.event_json()
        if "start" in event and isinstance(event["start"],str):
            event["start"]=convert_datetime_from_str(event["start"]).timestamp()*1000
        if "end" in event and isinstance(event["end"],str):
            event["end"]=convert_datetime_from_str(event["end"]).timestamp()*1000
        final_result.append(event)
    return final_result
@profiler
def search_tag(data,session):
    longitude,latitude = data["longitude"],data["latitude"]
    start_time,end_time = None,None
    if data['start_time']:
        start_time = datetime.fromtimestamp(data['start_time']/1000)
    if data['end_time']:
        end_time = datetime.fromtimestamp(data['end_time']/1000)
    print(start_time, end_time)
    result             = search_by_tagging(longitude,latitude,data["radius"],data["search_word"],data["page"],data["size"],session,start_time=start_time,end_time=end_time)
    result = [event.event_json() for event in result]
    final_result       = []
    for event in result:
        current_date    = datetime.now().timestamp()
        if "start" in event and isinstance(event["start"],str):
            event["start"]=convert_datetime_from_str(event["start"]).timestamp()*1000
        if "end" in event and isinstance(event["end"],str):
            event["end"]=convert_datetime_from_str(event["end"]).timestamp()*1000
        __check_bookable(current_date,event)
        final_result.append(event)
    return final_result

@profiler
def query_categories(session):
    return query_categories_tag(session)

@profiler
def generate_report(data,session):
    event_title,attendees = get_event_attendees(data['event_id'],session)
    column    = ("Ticket Id","First Name","Last Name","Email","Phone","Quantity")
    rows    = []
    count=0
    for attend in attendees:
        row = (attend.reg_unique_id,attend.reg_first_name,attend.reg_last_name,attend.reg_email,attend.reg_phone_number,attend.quantity)
        rows.append(row)
        count+=1
    transaction_id = str(uuid.uuid4())
    file_name=f'{transaction_id}.csv'
    report_data = create_csv_report(rows,column)
    html_data ={'customer_amount':count,'event_title':event_title}
    html_result = get_email_html('report.html',**html_data)
    upload_data = {"document_path":f"documents/{file_name}","event_id":data["event_id"],
                   "document_local_path":file_name,"user_id":data["user_id"],"transaction_id":transaction_id,"csv_datas":report_data}
    path = upload_document(upload_data)
    send_email_with_attachement(html_result,"Attendees Report","noreply@thekilimanjaroapp.com",data['send_email'],path,"text/csv")


def check_valid_for_refund(de_registration_data,event:Event):
    if not de_registration_data['paid']:
        return False
    start_date = event.event_start_time
    curr_date  = datetime.now()
    date_diff  = curr_date-start_date
    if date_diff.days<1:
        return False
    return True

__all__ = ("create_service_event",
           "search_by_suggestion",
           "query_categories",
           "search_pagination","home_page_search","search_tag",
           "find_event_by_id","register_user_for_event","generate_report", "cancel_event_registrations",
           "get_user_tickets","deregister_event","get_ticket",
           "user_valid_for_ticket","catch_all_transaction","email_trigger")
